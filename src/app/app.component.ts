import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import * as authActions from './auth/store/actions';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrl: './app.component.css'
})
export class AppComponent implements OnInit {
  title = 'ngrx-scratch';

  constructor(private store: Store) {}

  ngOnInit(): void {
    this.store.dispatch(authActions.currentUser())
  }
}
