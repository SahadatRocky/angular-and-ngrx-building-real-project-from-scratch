import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { PersistanceService } from '../auth/services/persistance.service';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  constructor(private persistanceService : PersistanceService){}
  intercept(request: HttpRequest<any>,next: HttpHandler) : Observable<HttpEvent<any>> {
    // Add your custom logic here
    // For example, add an Authorization header with a token
    const token = this.persistanceService.get('accessToken')
    const modifiedRequest = request.clone({
      setHeaders: {
        Authorization: token ? `Token ${token}` : '',
      },
    });

    // Pass the modified request to the next handler
    return next.handle(modifiedRequest);
  }
}
