import { ActionReducerMap } from '@ngrx/store';
import { AuthStateInterface } from '../types/auth-state.interface';
import * as authReducer from './reducers';

export interface AuthReducers {
  auth: AuthStateInterface;
}

export const reducers: ActionReducerMap<AuthReducers> = {
  auth: authReducer.authReducer,
};
