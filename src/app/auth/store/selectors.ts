import { createFeatureSelector, createSelector } from '@ngrx/store';
import { AuthStateInterface } from '../types/auth-state.interface';

export const selectAuthState = createFeatureSelector<AuthStateInterface>('auth');

export const selectIsSubmitting = createSelector(selectAuthState, (state) => state.isSubmitting);
export const selectIsLoading = createSelector(selectAuthState, (state) => state.isLoading);
export const selectCurrentUser = createSelector(selectAuthState, (state) => state.currentUser);
export const selectValidationErrors = createSelector(selectAuthState, (state) => state.validationErrors);
