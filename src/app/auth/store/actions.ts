import { createAction, props } from '@ngrx/store';
import { RegisterRequestInterface } from '../types/registerRequest.interface';
import { CurrentUserInterface } from '../../shared/types/currentUser.interface';
import { BackendErrorsInterface } from '../../shared/types/backendErrors.interface';
import { LoginRequestInterface } from '../types/loginRequest.interface';

export const register = createAction('[Auth] Register', props<{ request: RegisterRequestInterface }>());
export const registerSuccess = createAction('[Auth] Register Success', props<{ currentUser: CurrentUserInterface }>());
export const registerFailure = createAction('[Auth] Register Failure', props<{ validationErrors : BackendErrorsInterface }>());


export const login = createAction('[Auth] Login', props<{ request: LoginRequestInterface }>());
export const loginSuccess = createAction('[Auth] Login Success', props<{ currentUser: CurrentUserInterface }>());
export const loginFailure = createAction('[Auth] Login Failure', props<{ validationErrors : BackendErrorsInterface }>());


export const currentUser = createAction('Get current user');
export const currentUserSuccess = createAction('Get current user success', props<{ currentUser: CurrentUserInterface }>());
export const currentUserFailure = createAction('Get current user failure');
