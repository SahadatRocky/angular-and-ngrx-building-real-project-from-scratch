import { createReducer, on } from '@ngrx/store';
import { AuthStateInterface } from '../types/auth-state.interface';
import * as authActions from './actions';

export const initialState: AuthStateInterface = {
  isSubmitting: false,
  isLoading: false,
  currentUser: undefined,
  validationErrors: null,
};


export const authReducer = createReducer(
  initialState,
  on(authActions.register, (state) => ({ ...state, isSubmitting: true, validationErrors : null})),
  on(authActions.registerSuccess, (state, { currentUser }) => ({ ...state, isSubmitting: false, currentUser })),
  on(authActions.registerFailure, (state, { validationErrors }) => ({ ...state, isSubmitting: false, validationErrors })),
  on(authActions.login, (state) => ({ ...state, isSubmitting: true, validationErrors : null})),
  on(authActions.loginSuccess, (state, { currentUser }) => ({ ...state, isSubmitting: false, currentUser })),
  on(authActions.loginFailure, (state, { validationErrors }) => ({ ...state, isSubmitting: false, validationErrors })),

  on(authActions.currentUser, (state) => ({...state, isLoading: true})),
  on(authActions.currentUserSuccess, (state, action) => ({...state,isLoading: false,currentUser: action.currentUser})),
  on(authActions.currentUserFailure, (state) => ({...state,isLoading: false,currentUser: null})),
);

// export const getIsSubmitting = (state: AuthStateInterface) => state.isSubmitting;
// export const getIsLoading = (state: AuthStateInterface) => state.isLoading;
// export const getCurrentUser = (state: AuthStateInterface) => state.currentUser;
// export const getValidationErrors = (state: AuthStateInterface) => state.validationErrors;
