import { Injectable } from "@angular/core";
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { AuthService } from "../services/auth.service";
import { catchError, map, switchMap, tap } from "rxjs/operators";
import * as authActions from "./actions";
import { of } from 'rxjs';
import { PersistanceService } from "../services/persistance.service";
import { Router } from "@angular/router";
import { CurrentUserInterface } from "../../shared/types/currentUser.interface";

@Injectable()
export class AuthEffects {

  constructor(private actions$: Actions,
    private authService: AuthService,
    private persistanceService : PersistanceService,
    private router : Router) { }


    getCurrentUserEffect$ = createEffect(() =>
     this.actions$.pipe(
          ofType(authActions.currentUser),
          switchMap(() => {
            const token = this.persistanceService.get('accessToken')

            if (!token) {
              return of(authActions.currentUserFailure())
            }
            return this.authService.getCurrentUser().pipe(
              map((currentUser: CurrentUserInterface) => {
                return authActions.currentUserSuccess({currentUser})
              }),
              catchError(() => {
                return of(authActions.currentUserFailure())
              })
            )
          })
        )
      );



  registerEffect$ = createEffect(() =>
    this.actions$.pipe(
      ofType(authActions.register),
      tap(() => { console.log('new register occurred in queue') }),
      switchMap((action) => {
        console.log('new running')
        return this.authService.register(action.request).pipe(
          map((res) => {
            console.log('current User res:::',res);
            this.persistanceService.set('accessToken', res.token)
            return authActions.registerSuccess({ currentUser: res }) }),
          catchError((validationErrors) => of(authActions.registerFailure({ validationErrors : validationErrors.error.errors }))),
          tap(() => { console.log('regiter Finished')})
        )
      })
    )
  );

  loginEffect$ = createEffect(() =>
    this.actions$.pipe(
      ofType(authActions.login),
      tap(() => { console.log('new login occurred in queue') }),
      switchMap((action) => {
        console.log('new running')
        return this.authService.login(action.request).pipe(
          map((res) => {
            console.log('current User res:::',res);
            this.persistanceService.set('accessToken', res.token)
            this.router.navigateByUrl('/')
            return authActions.loginSuccess({ currentUser: res }) }),

          catchError((validationErrors) => of(authActions.loginFailure({ validationErrors : validationErrors.error.errors }))),
          tap(() => { console.log('login Finished')})
        )
      })
  )
);






}


