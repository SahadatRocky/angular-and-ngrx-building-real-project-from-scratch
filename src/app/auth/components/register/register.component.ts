import {Component, OnInit} from '@angular/core'
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { RegisterRequestInterface } from '../../types/registerRequest.interface';
import { Observable } from 'rxjs';
import { selectIsSubmitting,selectValidationErrors } from '../../store/selectors';
import { AuthService } from '../../services/auth.service';
import * as authActions from '../../store/actions'
import { BackendErrorsInterface } from '../../../shared/types/backendErrors.interface';
import { Router } from '@angular/router';

@Component({
  selector: 'mc-register',
  templateUrl: './register.component.html'
})
export class RegisterComponent implements OnInit {

  form!: FormGroup;
  isSubmitting$ : Observable<boolean>;
  isValidationErrors !: BackendErrorsInterface | null;

  constructor(
    private authService : AuthService,
    private router: Router,
    private store: Store) {
    this.isSubmitting$ = this.store.select(selectIsSubmitting);
    this.store.select(selectValidationErrors).subscribe(res =>{
      console.log('==>>>',res);
      this.isValidationErrors = res;
    });

  }

  ngOnInit(): void {
    this.buildForm();
  }

  buildForm() {
    this.form = new FormGroup({
      username: new FormControl('', [Validators.required]),
      email: new FormControl('', [Validators.required]),
      password: new FormControl('', [Validators.required])
    });
  }

  onSubmit() {
    console.log('form', this.form.getRawValue());
    const request: RegisterRequestInterface = {
      user: this.form.getRawValue()
    };
    this.store.dispatch(authActions.register({ request }));
  }

  isDisable() {
    //dirty check
    // return !(this.form.valid && (this.form.dirty || this.form.touched));
    console.log('===>>',this.form.valid);
    return !(this.form.valid);
  }

  navigateToLogin() {
    this.router.navigate(['/auth/login']);
  }

  get username(){
    return this.form.controls['username'];
  }

  get email(){
    return this.form.controls['email'];
  }

  get password(){
    return this.form.controls['password'];
  }

}
