import {CommonModule} from '@angular/common'
import {Component, OnInit} from '@angular/core'
import {FormBuilder, FormControl, FormGroup, ReactiveFormsModule, Validators} from '@angular/forms'
import {Router, RouterLink} from '@angular/router'
import {Store} from '@ngrx/store'
import {Observable, combineLatest} from 'rxjs'
import {LoginRequestInterface} from '../../types/loginRequest.interface'
import { BackendErrorsInterface } from '../../../shared/types/backendErrors.interface'
import { AuthService } from '../../services/auth.service'
import { selectIsSubmitting, selectValidationErrors } from '../../store/selectors'
import * as authActions from '../../store/actions'

@Component({
  selector: 'mc-login',
  templateUrl: './login.component.html'
})
export class LoginComponent  implements OnInit{

  form!: FormGroup;
  isSubmitting$ : Observable<boolean>;
  isValidationErrors !: BackendErrorsInterface | null;

  constructor(
    private authService : AuthService,
    private router: Router,
    private store: Store) {
    this.isSubmitting$ = this.store.select(selectIsSubmitting);
    this.store.select(selectValidationErrors).subscribe(res =>{
      console.log('==>>>',res);
      this.isValidationErrors = res;
    });

  }

  ngOnInit(): void {
    this.buildForm();
  }

  buildForm() {
    this.form = new FormGroup({
      email: new FormControl('', [Validators.required]),
      password: new FormControl('', [Validators.required])
    });
  }

  onSubmit() {
    console.log('form', this.form.getRawValue());
    const request: LoginRequestInterface = {
      user: this.form.getRawValue()
    };
    this.store.dispatch(authActions.login({ request }));
  }

  isDisable() {
    //dirty check
    // return !(this.form.valid && (this.form.dirty || this.form.touched));
    console.log('===>>',this.form.valid);
    return !(this.form.valid);
  }

  navigateToRegister() {
    this.router.navigate(['/auth/register']);
  }

  get email(){
    return this.form.controls['email'];
  }

  get password(){
    return this.form.controls['password'];
  }
}
