import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { RegisterRequestInterface } from "../types/registerRequest.interface";
import { Observable, map } from "rxjs";
import { CurrentUserInterface } from "../../shared/types/currentUser.interface";
import { AuthResponseInterface } from "../types/authResponse.interface";
import { LoginRequestInterface } from "../types/loginRequest.interface";

@Injectable({
  providedIn: 'root'
})
export class AuthService{

  constructor(private http: HttpClient) { }

  getCurrentUser(): Observable<CurrentUserInterface> {
    const url = 'https://api.realworld.io/api/user'
    return this.http.get<AuthResponseInterface>(url).pipe(map(response => response.user))
  }

  register(data: RegisterRequestInterface): Observable<CurrentUserInterface> {
    console.log("register calling...")
    const url = 'https://api.realworld.io/api/users';
    return this.http.post<AuthResponseInterface>(url, data)
      .pipe(
        map((response) => response.user)
      );
  }


  login(data: LoginRequestInterface): Observable<CurrentUserInterface> {
    const url = 'https://api.realworld.io/api/users/login'
    return this.http
      .post<AuthResponseInterface>(url, data)
      .pipe(map(response =>  response.user))
  }

}
