import {Component} from '@angular/core'
import {Store} from '@ngrx/store'
import {combineLatest} from 'rxjs'
import { selectCurrentUser } from '../../auth/store/selectors'

@Component({
  selector: 'mc-topbar',
  templateUrl: './topBar.component.html'
})
export class TopBarComponent {
  data$ = combineLatest({
    currentUser: this.store.select(selectCurrentUser),
  })
  constructor(private store: Store) {}
}
