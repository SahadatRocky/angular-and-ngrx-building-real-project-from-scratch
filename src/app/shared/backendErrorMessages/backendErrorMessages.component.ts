import {CommonModule} from '@angular/common'
import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core'
import {BackendErrorsInterface} from '../types/backendErrors.interface'

@Component({
  selector: 'mc-backend-error-messages',
  templateUrl: './backendErrorMessages.component.html',
})
export class BacknedErrorMessages implements OnChanges {

  @Input() backendErrors: BackendErrorsInterface = {};

  errorMessages: string[] = []

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges): void {
    console.log('backendErrors::',this.backendErrors);
    if (this.backendErrors) {
      this.errorMessages = Object.keys(this.backendErrors).map((name: string) => {
        const errorValue = this.backendErrors[name];
        if (Array.isArray(errorValue)) {
          const messages = errorValue.join(' ');
          return `${name} ${messages}`;
        } else {
          // Handle non-array error value (if needed)
          return `${name} ${errorValue}`;
        }
      });
    }
  }

}
